<?php
/*
Un fichier vide « private » fait qu'un dossier n'est pas affiché dans la liste, il faut avoir l'URL pour voir la galerie associée.
Un fichier « comment.txt » va mettre une commentaire à un dossier
Un fichier <nom du fichier image>.txt va mettre un commentaire à une image.
Les images se mettent dans le dossier data/.
*/
define('VERSION', 20180729);
error_reporting(0);
function scan_dir($dir) {
	$array = array();
	foreach(scandir($dir) as $file) {
		if($file != '.' AND $file != '..') {
			$array[] = $file;
		}
	}
	return $array;
}
function Size($path) { #http://stackoverflow.com/questions/5501427/php-filesize-mb-kb-conversion
    $bytes = sprintf('%u', filesize($path));
    if ($bytes > 0) {
        $unit = intval(log($bytes, 1024));
        $units = array('B', 'KB', 'MB', 'GB');
        if (array_key_exists($unit, $units) === true) {
            $size = sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
        }
    }

    return array('bytes'=>$bytes, 'size'=>$size);
}
function date_in_french($time) {
	$week_name = array("dimanche","lundi","mardi","mercredi","jeudi","vendredi","samedi");
	$month_name=array("","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre");
	$date = date('Y-m-d', $time);
	$split = preg_split('/-/', $date);
	$year = $split[0];
	$month = round($split[1]);
	$day = round($split[2]);
	$hour = date('H:i:s', $time);
	$week_day = date("w", mktime(12, 0, 0, $month, $day, $year));
	return $date_fr = $week_name[$week_day] .' '. $day .' '. $month_name[$month] .' '. $year.', '.$hour;
}
function folderSize($path) { #https://gist.github.com/eusonlito/5099936
	$bytes = 0;
	foreach (glob(rtrim($path, '/').'/*', GLOB_NOSORT) as $each) {
		$bytes += is_file($each) ? filesize($each) : folderSize($each);
	}
	 if ($bytes > 0) {
        $unit = intval(log($bytes, 1024));
        $units = array('B', 'KB', 'MB', 'GB');
        if (array_key_exists($unit, $units) === true) {
            $size = sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
        }
    }
   return array('bytes'=>$bytes, 'size'=>$size);
}

function getGps($exifCoord, $hemi) { # https://stackoverflow.com/questions/2526304/php-extract-gps-exif-data
    $degrees = count($exifCoord) > 0 ? gps2Num($exifCoord[0]) : 0;
    $minutes = count($exifCoord) > 1 ? gps2Num($exifCoord[1]) : 0;
    $seconds = count($exifCoord) > 2 ? gps2Num($exifCoord[2]) : 0;
    $flip = ($hemi == 'W' or $hemi == 'S') ? -1 : 1;
    return $flip * ($degrees + $minutes / 60 + $seconds / 3600);
}
function gps2Num($coordPart) {
    $parts = explode('/', $coordPart);
    if (count($parts) <= 0)
        return 0;
    if (count($parts) == 1)
        return $parts[0];
    return floatval($parts[0]) / floatval($parts[1]);
}
function getCamera($picture) {
	$array = array();
	$exif = exif_read_data($picture);
	GetImageSize ($picture,$info);
	$iptc = iptcparse ($info["APP13"]);
   	$array['date'] = isset($exif['DateTimeOriginal']) ? date_in_french(strtotime($exif['DateTimeOriginal'])) : false;
	$array['make'] = isset($exif['Make']) ? $exif['Make'] : false;
	$array['model'] = isset($exif['Model']) ? $exif['Model'] : false;
	$array['aperture'] = isset($exif['COMPUTED']['ApertureFNumber']) ? $exif['COMPUTED']['ApertureFNumber'] : false;
	$array['exposure'] = isset($exif['ExposureTime']) ? $exif['ExposureTime'] : false;
	$array['iso'] = isset($exif['ISOSpeedRatings']) ? $exif['ISOSpeedRatings'] : false;
	$array['lat'] = isset($exif['GPSLatitude']) ? getGps($exif['GPSLatitude'], $exif['GPSLatitudeRef']) : false;
	$array['lon'] = isset($exif['GPSLongitude']) ? getGps($exif['GPSLongitude'], $exif['GPSLongitudeRef']) : false;
	$array['resolution'] = isset($exif['COMPUTED']['Height']) ? $exif['COMPUTED']['Width'].'×'.$exif['COMPUTED']['Height'] : false;
	$array['focal'] = isset($exif['FocalLength']) ? $exif['FocalLength'] : false;
	$array['flash'] = isset($exif['Flash']) ? (($exif['Flash'] & 1) != 0) ? 'Flash activé' : 'Flash désactivé' : false;
	$array['title'] = isset($iptc["2#105"][0]) ? $iptc["2#105"][0] : false;
	$array['author'] = isset($iptc["2#122"][0]) ? '© '.$iptc["2#122"][0] : false;
	return $array;
}
function make_thumb($src, $dest, $desired_width) { # https://davidwalsh.name/create-image-thumbnail-php
	if(!file_exists($dest)) {
		if(mime_content_type($src) == 'image/jpeg') {
			$source_image = imagecreatefromjpeg($src);
			$width = imagesx($source_image);
			$height = imagesy($source_image);
			$desired_height = floor($height * ($desired_width / $width));
			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			imagejpeg($virtual_image, $dest);
			return $dest;
		}
		elseif(mime_content_type($src) == 'image/png') {
			$source_image = imagecreatefrompng($src);
			$width = imagesx($source_image);
			$height = imagesy($source_image);
			$desired_height = floor($height * ($desired_width / $width));
			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			imagepng($virtual_image, $dest);
			return $dest;
		}
		elseif(mime_content_type($src) == 'image/gif') {
			$source_image = imagecreatefromgif($src);
			$width = imagesx($source_image);
			$height = imagesy($source_image);
			$desired_height = floor($height * ($desired_width / $width));
			$virtual_image = imagecreatetruecolor($desired_width, $desired_height);
			imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
			imagegif($virtual_image, $dest);
			return $dest;
		}
		else {
			return $src;
		}
	}
	else {
		return $dest;
	}
}
function is_picture($file) {
	if(
		mime_content_type($file) == 'image/bmp' ||
		mime_content_type($file) == 'image/gif' ||
		mime_content_type($file) == 'image/jpeg' ||
		mime_content_type($file) == 'image/svg+xml' ||
		mime_content_type($file) == 'image/tiff' ||
		mime_content_type($file) == 'image/png') {
		return true;
	}
	else {
		return false;
	}
}
function is_audio($file) {
	if(
		mime_content_type($file) == 'audio/aac' ||
		mime_content_type($file) == 'audio/midi' ||
		mime_content_type($file) == 'audio/ogg' ||
		mime_content_type($file) == 'audio/mp3' ||
		mime_content_type($file) == 'audio/mpeg' ||
		mime_content_type($file) == 'audio/x-wav' ||
		mime_content_type($file) == 'audio/webm' ||
		mime_content_type($file) == 'audio/3gpp' ||
		mime_content_type($file) == 'audio/3gpp3') {
		return true;
	}
	else {
		return false;
	}
}
function is_video($file) {
	if(
		mime_content_type($file) == 'video/x-msvideo' ||
		mime_content_type($file) == 'video/mpeg' ||
		mime_content_type($file) == 'video/ogg' ||
		mime_content_type($file) == 'video/webm' ||
		mime_content_type($file) == 'video/mp4' ||
		mime_content_type($file) == 'video/3gpp' ||
		mime_content_type($file) == 'video/3gpp2') {
		return true;
	}
	else {
		return false;
	}
}
function is_office($file) {
	if(
		mime_content_type($file) == 'application/x-abiword' ||
		mime_content_type($file) == 'application/octet-stream' ||
		mime_content_type($file) == 'application/vnd.amazon.ebook' ||
		mime_content_type($file) == 'application/x-bzip' ||
		mime_content_type($file) == 'application/x-bzip2' ||
		mime_content_type($file) == 'application/x-csh' ||
		mime_content_type($file) == 'text/csv' ||
		mime_content_type($file) == 'application/msword' ||
		mime_content_type($file) == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
		mime_content_type($file) == 'application/vnd.ms-fontobject' ||
		mime_content_type($file) == 'application/epub+zip' ||
		mime_content_type($file) == 'text/calendar' ||
		mime_content_type($file) == 'application/json' ||
		mime_content_type($file) == 'application/vnd.oasis.opendocument.presentation' ||
		mime_content_type($file) == 'application/vnd.oasis.opendocument.spreadsheet' ||
		mime_content_type($file) == 'application/vnd.oasis.opendocument.text' ||
		mime_content_type($file) == 'font/otf' ||
		mime_content_type($file) == 'application/vnd.ms-powerpoint' ||
		mime_content_type($file) == 'application/vnd.openxmlformats-officedocument.presentationml.presentation' ||
		mime_content_type($file) == 'application/x-rar-compressed' ||
		mime_content_type($file) == 'application/rtf' ||
		mime_content_type($file) == 'application/vnd.ms-excel' ||
		mime_content_type($file) == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ||
		mime_content_type($file) == 'application/xml' ||
		mime_content_type($file) == 'application/zip' ||
		mime_content_type($file) == 'application/x-7z-compressed') {
		return true;
	}
	else {
		return false;
	}
}
function is_pdf($file) {
	if(mime_content_type($file) == 'application/pdf') {
		return true;
	}
	else {
		return false;
	}
}
########
if(!is_dir('data')) {mkdir('data');}
if(!is_dir('cache')) {mkdir('cache');}
if(!file_exists('data/config.php')) {
	$config['name'] = 'Gallery';
	$config['color'] = '#fab';
	$config['root'] = str_replace('/index.php','','//'.$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
	file_put_contents('data/config.php', json_encode($config));
}
else {
	$config = json_decode(file_get_contents('data/config.php'), true);
}
if(!file_exists('.htaccess')) {
	file_put_contents('.htaccess', '<IfModule mod_rewrite.c> 
	RewriteEngine On
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^(.*)$  index.php?p=$1 [L]
	# RewriteCond %{HTTPS} off
	# RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
</IfModule>');

}
if(!file_exists('data/index.html')) {
	file_put_contents('data/index.html', '<html><head><meta http-equiv="refresh" content="0;URL='.$config['root'].'"></head><body></body></html>');

}
if(isset($_GET['u'])) {file_put_contents('index.php', file_get_contents('https://framagit.org/champlywood/petites-pousses/galerie/raw/master/index.php'));}
$_GET['p'] = (isset($_GET['p']) AND file_exists('data/'.$_GET['p'])) ? trim($_GET['p'], '/') : '';
?>
<!doctype html>
<html lang="fr">
<head>
<style>
main {
	width: 95%;
	margin:auto;
}
.block {
   order: 0;
	flex: 0 1 auto;
	align-self: auto;
	flex-direction: column;
	display: inline-flex;
	border: 1px solid #e5e5e5;
	width:150px;
	height:150px;
	padding: 10px;
	margin:10px;
	box-sizing:border-box;
	vertical-align:middle;
	overflow: hidden;
	text-align:center;
	justify-content: center;
	align-items: center;
	background:#fff;
}
.content, .buttons {
	border: 1px solid #e5e5e5;
	background:#fff;
	padding: 20px;
	margin:10px;
	box-sizing:border-box;
}
.private {
	border: 1px solid #e5e5e5;
	background:#e32636;
	padding: 20px;
	margin:10px;
	box-sizing:border-box;
	text-align:center;
}
:root {
  --main-color: <?php echo $config['color']; ?>;
}
body,
html {
    margin: 0;
    padding: 0;
    background: #eee;
    color: rgba(0, 0, 0, .87);
    font-family: 'RobotoRegular', -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Arial, "Open Sans", "Noto Sans", "Fira Sans", "Droid Sans", Cantarell, "Helvetica Neue", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Emoji Regular";
    word-wrap: break-word;
    font-size: 1em;
}
a {
    color: #4496cc;
    text-decoration: none
}

a:hover {
    color: #337199;
    text-decoration: none
}
a.empty {
	color:#cc0000;
}
a.empty:hover {
	color:#990000;
}
nav, footer {
    background: var(--main-color);
    color: #fff;
    min-height: 64px;
    padding: 0 15px;
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.25);
    font-weight: 500;
    font-size: 1em;
    margin: auto;
    display: flex;
    align-items: center;
    justify-content: space-around;
    z-index: 999;
}
nav > a, footer > a {
    padding: 10px;
    color: #fff;
    text-decoration: none
}
nav > a:hover, footer a:hover {
    border-bottom: 3px solid #fff;
    color: #fff;
    text-decoration: none
}
hr {
    border: solid;
    border-width: 1px 0 0;
    box-sizing: content-box;
    height: 0;
    margin: 1.5em 0
}

aside {
  display: grid;
  grid-template-columns: 50% 50%;
}
aside .metadata {
  grid-column: 1;
}
aside .comment {
  grid-column: 2;
}

.view img, .view audio, .view video {
display:block;
margin:auto;
margin-top:10px;
max-width:50%;
}
.buttons {
    display: flex;
    align-items: center;
    justify-content: space-around;
}
a[rel="prev"], a[rel="next"] {
	font-size:2em;
	display:align-block;
	vertical-align:middle;
}
@media (max-width: 800px) {
  aside {
    display:block;
  }
  aside .metadata, aside .comment {
	width:100%;
	}
}
</style>
	<link href="data/style.css" type="text/css" rel="stylesheet" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
	<meta charset="utf-8"/>
	<title><?php echo $config['name'].' '.$_GET['p']; ?></title>
</head>
<body>
<nav>
	<a href="<?php echo $config['root']; ?>"><?php echo $config['name']; ?></a>
<?php
if(isset($_GET['p']) AND $_GET['p'] != '') {
	$parts = explode('/', trim(pathinfo($_GET['p'])['dirname'],'/'));
	$crumb = '';
	foreach($parts as $part) {
		$crumb .= '/' . $part;
			echo '<a href="'.$config['root'].'/'.trim($crumb, '/').'">'.$crumb.'</a>';
	}
}
?>
</nav>
<main>
<?php
if(!file_exists('data/'.$_GET['p'])) {header('Location: index.php?p=/'); }
$p = ($_GET['p'] =='/' OR $_GET['p']  == '') ? 'data/' : trim('data/'.$_GET['p'], '/');
if(is_dir($p)) {
	if(file_exists($p.'/private')) {
		echo '<div class="private">&#128274; Ce dossier est privé, merci de ne pas partager !</div>';
	}
	if(file_exists($p.'/comment.txt') AND file_get_contents($p.'/comment.txt') != '') {
		echo '<div class="content comment">'.file_get_contents($p.'/comment.txt').'</div>';
	}
	else {
		file_put_contents($p.'/comment.txt', '');
	}
	if(!file_exists($p.'/index.html')) {
		file_put_contents($p.'/index.html', '<html><head><meta http-equiv="refresh" content="0;URL='.$config['root'].'"></head><body></body></html>');
	}
	foreach(scan_dir($p) as $content) {
		if(is_dir($p.'/'.$content)) {
			if(!file_exists($p.'/'.$content.'/private')) {
				echo '<a href="'.$config['root'].'/'.trim($_GET['p'].'/'.$content, '/').'" class="block">'.$content.'</a>';
			}
		}
		elseif(is_picture($p.'/'.$content)) {
			echo '<a href="'.$config['root'].'/'.trim($_GET['p'].'/'.$content, '/').'" class="block"><img src="'.$config['root'].'/'.make_thumb($p.'/'.$content, 'cache/thumb_'.md5($p.'/'.$content), 200).'"/></a>';
		}
		elseif(is_audio($p.'/'.$content)) {
			echo '<a href="'.$config['root'].'/'.trim($_GET['p'].'/'.$content, '/').'" class="block"><span style="font-size:3em;">&#128266;</span><span style="font-size:.75em">'.$content.'</span></a>';
		}
		elseif(is_video($p.'/'.$content)) {
			echo '<a href="'.$config['root'].'/'.trim($_GET['p'].'/'.$content, '/').'" class="block"><span style="font-size:3em;">	
&#127916;</span><span style="font-size:.75em">'.$content.'</span></a>';
		}
		elseif(is_office($p.'/'.$content) || is_pdf($p.'/'.$content)) {
			echo '<a href="'.$config['root'].'/'.trim($_GET['p'].'/'.$content, '/').'" class="block"><span style="font-size:3em;">	
&#128456;</span><span style="font-size:.75em">'.$content.'</span></a>';
		}
		else {
			//echo mime_content_type($p.'/'.$content).' ';
		}
	}
}
else {
	$nav = array();
	foreach(scan_dir(pathinfo($p)['dirname']) as $file) {
		if(is_picture(pathinfo($p)['dirname'].'/'.$file) || is_audio(pathinfo($p)['dirname'].'/'.$file) || is_video(pathinfo($p)['dirname'].'/'.$file) ) {
			$nav[] = $file;
		}
	}
	$prev = pathinfo($_GET['p'])['dirname'].'/'.$nav[array_search(pathinfo($_GET['p'])['basename'],$nav)-1];
	$all = pathinfo($_GET['p'])['dirname'];
	$next = pathinfo($_GET['p'])['dirname'].'/'.$nav[array_search(pathinfo($_GET['p'])['basename'],$nav)+1];

	echo '<div class="view">';
	if(is_picture($p)) {
		echo '<a href="'.$config['root'].'/'.$p.'"><img src="'.$config['root'].'/'.$p.'"/></a>';
	}
	elseif(is_audio($p)) {
		echo '<a href="'.$config['root'].'/'.$p.'"><audio controls src="'.$config['root'].'/'.$p.'"></audio></a>';
	}
	elseif(is_video($p)) {
		echo '<a href="'.$config['root'].'/'.$p.'"><video controls src="'.$config['root'].'/'.$p.'"></video></a>';
	}
	elseif(is_office($p)) {
		echo '<a href="'.$config['root'].'/'.$p.'"><span style="font-size:3em;">&#8615;</span> '.pathinfo($p)['basename'].'</a>';
	}
	elseif(is_pdf($p)) {
		echo '<a href="'.$config['root'].'/'.$p.'"><span style="font-size:3em;">&#8615;</span> '.pathinfo($p)['basename'].'</a>
				<iframe src="'.$config['root'].'/'.$p.'" style="width:100%;height:100vh"></iframe>';
	}
	else {
	}
	echo '</div>';
	echo '<div class="buttons">';
	if(isset($prev) AND $prev != $all.'/') {echo '<a href="'.$config['root'].'/'.trim($prev, '/').'" rel="prev"> < </a>';}
	if(isset($next) AND $next !=$all.'/') {echo '<a href="'.$config['root'].'/'.trim($next, '/').'" rel="next"> > </a>';}
	echo '</div>';
	//print_r(exif_read_data($p));
	echo '<aside>';
	echo '<div class="content metadata">';
	if(is_picture($p)) {
		$metadata = getCamera($p);
		echo 'Appareil : '.$metadata['make'].' '.$metadata['model'].'<br/>'.PHP_EOL;
		echo 'Ouverture : '.$metadata['aperture'].'<br/>'.PHP_EOL;
		echo 'Temps de pose : '.$metadata['exposure'].'<br/>'.PHP_EOL;
		echo 'Exposition Iso : '.$metadata['iso'].'<br/>'.PHP_EOL;
		echo 'Distance focale : '.$metadata['focal'].'<br/>'.PHP_EOL;
		echo 'Flash : '.$metadata['flash'].'<br/>'.PHP_EOL;
		echo 'Résolution : '.$metadata['resolution'].'<br/>'.PHP_EOL;
		echo 'Poids : '.Size($p)['size'].'<br/>'.PHP_EOL;
		echo 'Date du cliché : '.$metadata['date'].'<br/>'.PHP_EOL;
		echo 'Localisation : <a href="geo:'.$metadata['lat'].' '.$metadata['lon'].'">'.$metadata['lat'].' '.$metadata['lon'].'</a>'.PHP_EOL;
		if($metadata['lon'] != '') {
		    echo '<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox='.$metadata['lon'].'%2C'.$metadata['lat'].'%2C'.$metadata['lon'].'%2C'.$metadata['lat'].'&amp;layer=mapnik&amp;marker='.$metadata['lat'].'%2C'.$metadata['lon'].'" style="border: 1px solid black"></iframe>';
	}
	}
	elseif(is_audio($p)) {
		echo 'Nom : '.pathinfo($p)['basename'];
	}
	elseif(is_video($p)) {
		echo 'Nom : '.pathinfo($p)['basename'];
	}
	elseif(is_office($p)) {
		echo 'Nom : '.pathinfo($p)['basename'];
	}
	else {
	}
	echo '</div>';
	$comment = pathinfo($p)['dirname'].'/'.pathinfo($p)['filename'].'.txt';
	if(file_exists($comment) AND file_get_contents($comment) != '') { 
		echo '<div class="content comment">'.file_get_contents($comment).'</div>';
	}
	else {
		file_put_contents($comment, '');
	}
	echo '</aside>';
}
?>
</main>
<footer>
	<a href="https://framagit.org/champlywood/galerie">Galerie</a>
<script>
window.addEventListener("keydown", function (event) {
  if (event.defaultPrevented) {
    return; // Do nothing if the event was already processed
  }

  switch (event.key) {
    case "ArrowDown":
      // Do something for "down arrow" key press.
      break;
    case "ArrowUp":
      // Do something for "up arrow" key press.
      break;
    case "ArrowLeft":
      window.location = "<?php echo $config['root'].'/'.trim($prev, '/'); ?>";
      break;
    case "ArrowRight":
      window.location = "<?php echo $config['root'].'/'.trim($next, '/'); ?>";
      break;
    case "Enter":
      // Do something for "enter" or "return" key press.
      break;
    case "Escape":
      // Do something for "esc" key press.
      break;
    default:
      return; // Quit when this doesn't handle the key event.
  }

  // Cancel the default action to avoid it being handled twice
  event.preventDefault();
}, true);
</script>
</footer>
</body>
</html>